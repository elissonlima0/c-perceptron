#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Type for save dynamic matrix dimensions
typedef struct dyn_int_matrix
{
	int **base;
	unsigned long rows;
	unsigned long cols;
} dyn_int_matrix;

//Process the network for a single input array
//A single epoch must call this function several times
int processInput(dyn_int_matrix weights, int* input)
{

	int result = 0, k = 0;


	for ( int i = 0; i < weights.rows ; i++)
	{
		result += weights.base[i][0] * 1;
		for ( int j = 1 ; j < weights.cols ; j++)
		{
			result += weights.base[i][j] * input[k];
		       	k++;
		}
	}

	return result;
}

int net(int result)
{
	if( result >= 0)
		return 1;
	else
		return 0;
}

dyn_int_matrix createWeightsMatrix(int rows, int cols)
{
	dyn_int_matrix result;	
       	result.base = (int **)malloc(rows * sizeof(int*));
	result.rows = rows;
	result.cols = cols + 1; // Including extra column for bias

	srand(time(NULL));

	for (int i = 0 ; i < rows; i++)
	{
		result.base[i] = (int *)malloc(result.cols * sizeof(int));
		result.base[i][0] = rand() % 5;
		for (int j = 1; j < result.cols ; j++) result.base[i][j] = rand() % 5;
	}

	return result;
}

//Get file input and load it as int matrix
//This function expects a comma-separated or 'csv' file
//DO NOT add the bias value to input
//The last column is considered the target out value
dyn_int_matrix readFileAsArray(char* filePath)
{
	char character;
	dyn_int_matrix result;
	int buf = 0, column = 0;
	char* buffer = (char*) malloc(0 * sizeof(char));
	int* line = (int*) malloc(0 * sizeof(int));
	result.base = (int**) malloc(0 * sizeof(int*));
	result.rows = 0; result.cols = 0;

	FILE *fp = fopen(filePath, "r");

	if(fp == NULL) {
		printf("Could not open file in path '%s'\n", filePath);
		exit(-1);
	}

	while ( (character = getc(fp)) != EOF ) {
		if (character != ';' && character != '\n') {
			buffer = (char *) realloc(buffer, (buf+1) * sizeof(char));
			buffer[buf] = character;
			buf++;
		}
		else if(character == ';') {
			line = (int*) realloc(line, (column+1) * sizeof(int));
			line[column] = atoi(buffer);
			column++;
			buf = 0;
			free(buffer);
			buffer = (char*) malloc (0 * sizeof(char));
		}
		else if (character == '\n') {
			line[column] = atoi(buffer);
			buf = 0;
			free(buffer);
			buffer = (char*) malloc (0 * sizeof(char));

			result.base = (int**) realloc(result.base, (result.rows+1) * sizeof(int*));
			result.base[result.rows] = line;
			result.rows++;
			result.cols = column + 1;
			column = 0;
			line = (int*) malloc(0 * sizeof(int));
		}
	}

	fclose(fp);
	return result;

}

void changeWeight(dyn_int_matrix weights, int* input, int networkOut, int expectedOut, int learningRate) {

	int k = 0;

	for (int i = 0; i < weights.rows ; i++) {

		weights.base[i][0] = weights.base[i][0] + learningRate * ( expectedOut - networkOut ) * 1;
		for(int j = 1 ; j < weights.cols ; j++) {
			weights.base[i][j] = weights.base[i][j] + learningRate * ( expectedOut - networkOut ) * input[k];
			k++;
		}
	}	

}

//Train the network
//This functions modify the weight matrix passed by param 
void trainNetwork(dyn_int_matrix weights, dyn_int_matrix input, int epochs, int learningRate) {

	int networkResult, expected;

	for (int e = 0; e < epochs; e++) {

		for (int i = 0 ; i < input.rows ; i++) {

			networkResult = net(processInput(weights, input.base[i]));
			expected = input.base[i][input.cols - 1];
			//printf("Epoch[%d]: Network result: %d - Expected %d\n", e + 1, networkResult, expected);
			if (networkResult != expected) 
				changeWeight(weights, input.base[i], networkResult, expected, learningRate);

		}
	}

}

void printWeights(dyn_int_matrix weights) {

	for (int i = 0 ; i < weights.rows ; i++) {
		for ( int j = 0 ; j < weights.cols ; j ++) {
			printf("%d ", weights.base[i][j]);
		}
		printf("\n");
	}

}

void testNetworkForManualInput(dyn_int_matrix weights, int inputSize) {

	printf("Starting test cicle\n");

	char repeat = 'n';
	int* testInput;
	int networkResult;


	do {
		testInput = (int*) malloc( inputSize * sizeof(int));

		for (int i = 0 ; i < inputSize ; i++) {
			printf("Insert input n %d:", i + 1);
			scanf("%d", &testInput[i]);
		}
		
		networkResult = net(processInput(weights, testInput));
		printf("Result: %d\n", networkResult);
		printf("Continue?(y/n)");
		//fflush(stdout);
		scanf(" %c", &repeat);

	}while(repeat == 'y');

	printf("Bye!\n");
}

int main(int argc, char* argv[]) {

	dyn_int_matrix weights = createWeightsMatrix(1,2);
	dyn_int_matrix input = readFileAsArray("input.txt");

	if ( ( weights.cols - 1 ) != (input.cols - 1) ) {
		printf("Weight and Input matrices with incompatible size\n");
		//printf("%d - %d", (weights.cols - 1), (input.cols - 1));
		exit(-1);
	}
	printf("Inicial weights\n");
	printWeights(weights);

	trainNetwork(weights, input, 10, 1);

	printf("Final weights\n");
	printWeights(weights);

	testNetworkForManualInput(weights, (input.cols - 1));

	return 0;

}
